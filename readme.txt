Build image :
sudo docker build -t="opencds/jbpm:ubuntu" .

Create container :
sudo docker run --name jbpm -p 8080:8081 -i -t opencds/jbpm:ubuntu
Find out the ipaddres :
ifconfig

You can allow ssh on your container by running (root/screencast) :
/usr/sbin/sshd

Change the value of jboss.bind.address according to ipaddress assigned to the container (e.g.,):
<property name="jboss.bind.address" value="172.17.0.28" />

Run jbpm
cd jbpm-installer; ant start.demo.noeclipse

Check kie-drools in your browser (e.g.,) :
http://172.17.0.96:8080/jbpm-console/

References :
GitHub https://github.com/droolsjbpm/jbpm


