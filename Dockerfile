# Version: 0.0.1

FROM ubuntu:14.04
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# this is a non-interactive automated build - avoid some warning messages
ENV DEBIAN_FRONTEND noninteractive

# Install packages
ENV REFRESHED_AT 2015-01-29
RUN apt-get update && \
    apt-get install -yq --no-install-recommends git maven ant wget unzip pwgen ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# ssh 
RUN apt-get update && apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:screencast' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Install Java
RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main"\
    > /etc/apt/sources.list.d/webupd8team-java.list \
    && echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main"\
    >> /etc/apt/sources.list.d/webupd8team-java.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886 \
    && apt-get update -y \
    && echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections \
    && apt-get install -y oracle-java8-installer \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set java home	
RUN echo JAVA_HOME=/usr/lib/jvm/java-8-oracle >> ~/.bashrc
RUN echo PATH=$JAVA_HOME/bin:$PATH:$HOME/bin >> ~/.bashrc
RUN echo export PATH JAVA_HOME >> ~/.bashrc

# Set maven home
RUN echo export M2_HOME=/usr/share/maven >> ~/.bashrc

# Add opencds dependencies
ADD opencds-common-2.1.0-SNAPSHOT.jar /opencds-common-2.1.0-SNAPSHOT.jar
ADD opencds-vmr-1_0-internal-2.1.0-SNAPSHOT.jar /opencds-vmr-1_0-internal-2.1.0-SNAPSHOT.jar

RUN mvn install:install-file -Dfile=/opencds-common-2.1.0-SNAPSHOT.jar -DgroupId=org.opencds -DartifactId=opencds-common -Dversion=2.1.0-SNAPSHOT -Dpackaging=jar
RUN mvn install:install-file -Dfile=/opencds-vmr-1_0-internal-2.1.0-SNAPSHOT.jar -DgroupId=org.opencds -DartifactId=opencds-vmr-1_0-internal -Dversion=2.1.0-SNAPSHOT -Dpackaging=jar

RUN rm opencds-vmr-1_0-internal-2.1.0-SNAPSHOT.jar
RUN rm opencds-common-2.1.0-SNAPSHOT.jar

# add jbpm-installer
ADD jbpm-6.1.0.Final-installer-full.zip /jbpm-6.1.0.Final-installer-full.zip
RUN unzip jbpm-6.1.0.Final-installer-full.zip
RUN rm jbpm-6.1.0.Final-installer-full.zip
RUN cd jbpm-installer; ant install.demo.noeclipse

# Add opencds conf files
RUN mkdir .opencds 
#ADD guvnor-users.properties /.opencds/guvnor-users.properties
#ADD opencds.properties /.opencds/opencds.properties
ADD opencds-apelon.properties /.opencds/opencds-apelon.properties
#ADD opencds-guvnor.properties /.opencds/opencds-guvnor.properties

EXPOSE 8080
EXPOSE 8001
EXPOSE 9418
EXPOSE 22

CMD ["/bin/bash"]
